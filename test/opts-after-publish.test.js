const test = require('tape')

const SplitPublish = require('../')
const Server = require('./testbot')

function splitter (content) {
  const first = {
    type: 'post',
    text: content.text.slice(0, 4000), // janky!
    root: content.root || null
  }
  if (content.branch) first.branch = content.branch

  const second = {
    type: 'post',
    text: content.text.slice(4000), // janky!
    root: content.root,
    branch: ['TODO']
  }

  return [first, second]
}

function afterPublish (firstMsg, secondChunk) {
  secondChunk.root = firstMsg.value.content.root || firstMsg.key
  secondChunk.branch = [firstMsg.key]
  return secondChunk
}

test('opts.afterPublish', async t => {
  const ssb = Server()

  const publish = SplitPublish(ssb, splitter, { afterPublish })

  const largeContent = {
    type: 'post',
    text: [
      ...new Array(4000).fill('a'),
      ...new Array(5000).fill('b'),
      ...new Array(4000).fill('c')
    ].join(''),
    root: null
  }

  const msgs = await publish(largeContent)
  const expected = [
    {
      type: 'post',
      text: new Array(4000).fill('a').join(''),
      root: null
    },
    {
      type: 'post',
      text: new Array(4000).fill('b').join(''),
      root: msgs[0].key,
      branch: [msgs[0].key]
    },
    {
      type: 'post',
      text: [
        ...new Array(1000).fill('b'),
        ...new Array(4000).fill('c')
      ].join(''),
      root: msgs[0].key,
      branch: [msgs[1].key]
    }
  ]
  t.deepLooseEqual(
    msgs.map(m => m.value.content),
    expected,
    'can modify chunks based on the message prior to it'
  )

  t.end()
  ssb.close()
})
